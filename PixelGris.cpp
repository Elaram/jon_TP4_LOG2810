//
// Created by Gabriel Bernard
//

#include "PixelGris.h"

/**
* Classe définissant un pixel en nuances de gris
* @author Vincent Bolduc et Jonathan Plante
* @version 2.0
*/

PixelGris::PixelGris(){
}

PixelGris::PixelGris(unsigned int donnee) {
    if(donnee < VALEUR_MAX_PIXEL) {
        donnee_ = donnee;
    } else {
        donnee_ = VALEUR_MAX_PIXEL;
    }
}

PixelGris::~PixelGris() {}

unchar* PixelGris::convertirPixelCouleur() const {
	unchar *ret = new unchar[3];
	ret[0] = ret[1] = ret[2] = donnee_;
	return ret;
}

bool PixelGris::convertirPixelBN() const {
	return donnee_ > 127;
}

unchar PixelGris::convertirPixelGris() const {
	return donnee_;
}

unsigned int PixelGris::obtenirDonnee() const {
	return donnee_;
}

unchar PixelGris::obtenirR() const {
	return donnee_; 
}
unchar PixelGris::obtenirG() const {
	return donnee_;
}
unchar PixelGris::obtenirB() const {
	return donnee_;
}

void PixelGris::mettreEnNegatif() {
	donnee_ = 255 - (int)donnee_;
}
Pixel* PixelGris::retournerCopieProfonde() const{
	Pixel* newp = new PixelGris(donnee_);
	return newp;
}
