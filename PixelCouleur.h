//
// Created by Gabriel Bernard
//

#ifndef TP3_PIXEL_COULEUR_H
#define TP3_PIXEL_COULEUR_H

#include "Pixel.h"

enum Couleur { R=0, G=1, B=2 };  // R = 0, G = 1, B = 2
const int LONGUEUR_TABLEAU = 3;  // donnee_[3]
/**
* Classe d�finissant un pixel de couleur
 * @author Vincent Bolduc et Jonathan Plante
 * @version 2.0
*/
class PixelCouleur :
	virtual public Pixel
{
public:
	PixelCouleur();
	/**
	* Initialise un objet de type PixelCouleur avec les valeurs par d�faut
	*/
	PixelCouleur(const unchar& r, const unchar& g, const unchar& b);
	/**
	* Initialise un objet de classe PixelCouleur avec les valeurs d�finies par des param�tres
	* @param r,g,b Respectivement les niveaux de teinte rouge, verte et bleue du pixel de couleur
	*/
	~PixelCouleur();
	/**
	* Supprime un objet de classe PixelCouleur
	*/
	void modifierTeinteRouge(unchar d);
	/**
	* Modifie le niveau de teinte rouge d'un pixel de couleur.
	* @param d  L'augmentation ou la diminution d�sir�e de la teinte rouge
	* @return void (rien)
	*/
	void modifierTeinteVert(unchar d);
	/**
	* Modifie le niveau de teinte verte d'un pixel de couleur.
	* @param d  L'augmentation ou la diminution d�sir�e de la teinte verte
	* @return void (rien)
	*/
	void modifierTeinteBleue(unchar d);
	/**
	* Modifie le niveau de teinte bleue d'un pixel de couleur.
	* @param d  L'augmentation ou la diminution d�sir�e de la teinte bleue
	* @return void (rien)
	*/

    // Accesseur
   virtual unchar obtenirR() const;
   /**
   * Obtient le niveau de teinte rouge d'un pixel de couleur.
   * @return donnee_[Couleur::R] le niveau de teinte rouge du pixel
   */
   virtual unchar obtenirG() const;
   /**
   * Obtient le niveau de teinte verte d'un pixel de couleur.
   * @return donnee_[Couleur::G] le niveau de teinte verte du pixel
   */
   virtual unchar obtenirB() const;
   /**
   * Obtient le niveau de teinte bleue d'un pixel de couleur.
   * @return donnee_[Couleur::B] le niveau de teinte bleue du pixel
   */
	bool convertirPixelBN() const;
	/**
	* Convertit le pixel en objet PixelBN
	* @return true(vrai) si le pixel est blanc et false(faux) si le pixel est noir
	*/
	unchar convertirPixelGris() const;
	/**
	* Convertit le pixel en objet PixelGris
	* @return moyenne La valeur moyenne des teintes rouge, verte et bleue du pixel
	*/
	virtual Pixel* retournerCopieProfonde() const;
	/**
	* Fait une copie profonde d'un pixel de couleur
	* @return  newp un pointeur vers le pixel de couleur nouvellement cree en memoire
	*/
	virtual void mettreEnNegatif();
	/**
	* Inverse le niveau d'intensit� des teintes RGB d'un pixel de couleur
	* @return  void(rien)
	*/
private:
	unsigned int somme() const;

	unchar donnee_[LONGUEUR_TABLEAU];
};

#endif // TP3_PIXEL_COULEUR_H	