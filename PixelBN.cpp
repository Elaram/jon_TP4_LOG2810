//
// Created by Gabriel Bernard
//

#include "PixelBN.h"
/**
* Classe définissant un pixel noir ou blanc
* @author Vincent Bolduc et Jonathan Plante
* @version 2.0
*/

PixelBN::PixelBN(): donnee_(false){
}

PixelBN::PixelBN(bool p): donnee_(p){
}

PixelBN::~PixelBN() {}

unchar* PixelBN::convertirPixelCouleur() const {
    unchar valeur = (unchar)VALEUR_MIN_PIXEL;
    if(donnee_) {
        valeur = (unchar)VALEUR_MAX_PIXEL;
    }

	unchar* ret = new unchar[3];
	ret[0] = ret[1] = ret[2] = valeur;

	return ret;
}

unchar PixelBN::convertirPixelGris() const {
	return donnee_ ? VALEUR_MAX_PIXEL : VALEUR_MIN_PIXEL;
}

bool PixelBN::convertirPixelBN() const {
	return donnee_;
}

bool PixelBN::obtenirDonnee() const {
	return donnee_;
}

unchar PixelBN::obtenirR() const {
	if (donnee_)
		return (unchar)255;
	else
		return (unchar)0;
}
unchar PixelBN::obtenirG() const {
	if (donnee_)
		return (unchar)255;
	else
		return (unchar)0;
}
unchar PixelBN::obtenirB() const {
	if (donnee_)
		return (unchar)255;
	else
		return (unchar)0;
}

void PixelBN::mettreEnNegatif() {
	if (donnee_)
		donnee_ = false;
	else
		donnee_ = true;
}

Pixel* PixelBN::retournerCopieProfonde() const {
	Pixel* newp = new PixelBN(donnee_);
	return newp;
}
