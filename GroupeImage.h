//
// Created by Gabriel Bernard
//

#ifndef TP3_GROUPEIMAGE_H
#define TP3_GROUPEIMAGE_H

#include <vector>
#include <string>
#include "Image.h"
/*
* Classe définissant un groupe d'image
* @author Vincent Bolduc et Jonathan Plante
* @version 4.0
*/

class GroupeImage {
public:
    GroupeImage();
	/**
	* Initialise un objet GroupIamge avec les valeurs par defaut
	*/
    ~GroupeImage();
	/**
	* Supprime un objet GroupImage
	*/
    Image* obtenirImage(unsigned int indiceImage) const;
	/**
	* obtient l'image a l'endroit indique dans le groupe
	* @param indiceImage l'indice de l'image dans le groupe
	* @return images_[indiceImage] l'image a l'endroit specifie dans le groupe
	*/
    bool ajouterImage(Image* image);
	/**
	* obtient l'image a l'endroit indique dans le groupe
	* @param image un pointeur vers l'objet Image que l'on souhaite ajouter au groupe
	* @return true si l'image a bien ete ajoutee et false sinon
	*/
    bool retirerImage(const std::string& nom);
	/**
	* retire une image du groupe selon le nom specifie
	* @param nom le nom de l'image a retirer du groupe
	* @return true si l'image a bien ete retiree et false sinon
	*/
    void afficherImages(std::ostream& os) const; // Partie a modifier
	/**
	* affiche les images d'un groupe d'images
	* @return void(rien)
	*/
    GroupeImage& operator+=(Image* image);
	/**
	* Surcharge de l'operateur += pour ajouter une image sans avoir a appeler la methode concernee par son nom
	* @param image un pointeur vers l'objet Image que l'on souhaite ajouter au groupe
	* @return *this le groupe d'image courant
	*/
    GroupeImage& operator-=(Image* image);
	/**
	* Surcharge de l'operateur -= pour retirer une image sans avoir a appeler la methode concernee par son nom
	* @param image un pointeur vers l'objet Image que l'on souhaite retirer du groupe
	* @return *this le groupe d'image courant
	*/
	GroupeImage& operator=(const GroupeImage& groupe);
	/**
	* Surcharge de l'operateur d'affectation pour importer toutes les images d'un groupe dans un autre
	* @param groupe le groupe duquel on veut importer/copier toutes les images
	* @return *this le groupe d'image courant contenant maintenant les images importees
	*/
    friend std::ostream& operator<<(std::ostream& os, const GroupeImage& image);
	/**
	* Surcharge de l'operateur de flux de sortie pour afficher les iamges d'un groupe
	* @param image le groupe duquel on veut afficher les images
	* @return os la variable du flux de sortie contenant les informations sur chaque image du groupe
	*/
    Image* operator[](const unsigned int& indice) const;
	/**
	* Surcharge de l'operateur [] pour acceder aux images du groupe comme si le groupe etait de type tableau en memoire
	* @param indice l'emplacement de l'image dans le groupe
	* @return images_[indice] un pointeur vers une image si une image est a l'endroit specifie et 
	* nullptr s'il n'existe pas d'image a cet emplacement
	*/
    unsigned int obtenirNombreImages() const;
	/**
	* Obtient le nombre d'images dans le groupe
	* @return images_.size() le nombre d'images dans le groupe (la taille du groupe)
	*/
    void toutMettreEnNB();
	/**
	* Convertit toutes les images d'un groupe en images en noir et blanc
	* @return void(rien)
	*/
    void toutMettreEnGris();
	/**
	* Convertit toutes les images d'un groupe en images en niveau de gris
	* @return void(rien)
	*/
    void toutMettreEnCouleur();
	/**
	* Convertit toutes les images d'un groupe en images en couleur
	* @return void(rien)
	*/
	void toutMettreEnNegatif();
	/**
	* Inverse les niveaux des teintes des pixels des images du groupe
	* @return void(rien)
	*/
    void toutEnregistrer();
	/**
	* Enregistre sur le disque les images du groupe
	* @return void(rien)
	*/


    // void modifierImagesParIndices(std::vector<unsigned int>& vec, void (Image::*function_handler) ());

private:
    std::vector<Image*> images_;
};


#endif //TP3_GROUPEIMAGE_H
