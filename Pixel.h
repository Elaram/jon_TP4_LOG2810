

#ifndef TP3_PIXEL_H
#define TP3_PIXEL_H

typedef unsigned char unchar;

#define VALEUR_MAX_PIXEL 255
#define VALEUR_MIN_PIXEL 0

#include <iostream>
#include "TypePixel.h"

/**
* Classe définissant un pixel
* @author Vincent Bolduc et Jonathan Plante
* @version 4.0
*/
class Pixel
{
public:
    Pixel();
	/**
	* Initialise un objet de classe Pixel par défaut
	*/
	virtual ~Pixel();
	/**
	* Supprime un objet de classe Pixel
	*/
    unsigned int testPixel(const unsigned int& valeur) const;
	/**
	* Teste la valeur d'intensité de couleur d'un pixel lorsque donnée en type unsigned int
	* @param valeur la valeur d'intensité du pixel
	* @return  valeur la valeur d'intensité du pixel
	*/
    unchar testPixel(const unchar& valeur) const;
	/**
	* Teste la valeur d'intensité de couleur d'un pixel lorsque donnée en type unchar
	* @param valeur la valeur d'intensité du pixel 
	* @return valeur la valeur d'intensité du pixel
	*/
	virtual unchar obtenirR() const= 0;

	virtual unchar obtenirG() const= 0;

	virtual unchar obtenirB() const= 0;

	virtual bool operator==(const Pixel& pixel) const;
	/**
	* Surcharge de l'opérateur booléen pour permettre de vérifier que deux Pixel ont les mêmes intensités de teintes RGB
	* @return  true(vrai) si les pixels ont les memes valeurs RGB et false(faux) sinon
	*/
	virtual void mettreEnNegatif() = 0;
	/**
	* Méthode virtuelle pure qui permet d'inverser le niveau d'intensité d'une/de la teinte d'un pixel
	* @return  void(rien)
	*/
	virtual Pixel* retournerCopieProfonde() const = 0;
	/**
	* Méthode virtuelle pure qui permet de faire une deep copy d'un pixel
	* @return  Pixel* un pointeur vers le pixel nouvellement cree en memoire
	*/
	virtual bool convertirPixelBN() const = 0;
	/**
	* Méthode virtuelle pure qui convertit un pixel en un pixel noir ou blanc
	* @return  true(vrai) si le pixel est blanc et false(faux) si le pixel est noir
	*/
	virtual unchar convertirPixelGris() const = 0;
	/**
	* Méthode virtuelle pure qui convertit un pixel en un pixel en niveau de gris
	* @return  unchar la valeur d'intensité de gris du pixel
	*/
protected:
	//TypePixel type_;
	
};

#endif // PIXEL_H