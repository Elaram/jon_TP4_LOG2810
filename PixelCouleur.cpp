//
// Created by Gabriel Bernard
//

#include "PixelCouleur.h"

/**
* Classe définissant un pixel de couleur
* @author Vincent Bolduc et Jonathan Plante
* @version 2.0
*/

PixelCouleur::PixelCouleur() {
	donnee_[Couleur::R] = donnee_[Couleur::G] = donnee_[Couleur::B] = VALEUR_MIN_PIXEL;
}

PixelCouleur::PixelCouleur(const unchar& r, const unchar& g, const unchar& b) {
    donnee_[Couleur::R] = testPixel(r);
    donnee_[Couleur::G] = testPixel(g);
    donnee_[Couleur::B] = testPixel(b);
}

PixelCouleur::~PixelCouleur() {}

unchar PixelCouleur::obtenirR() const {
	return donnee_[Couleur::R];
}

unchar PixelCouleur::obtenirG() const {
    return donnee_[Couleur::G];
}

unchar PixelCouleur::obtenirB() const {
    return donnee_[Couleur::B];
}

unsigned int PixelCouleur::somme() const {
	unsigned int somme = donnee_[Couleur::R] + donnee_[Couleur::G] + donnee_[Couleur::B];
	return somme;
}

bool PixelCouleur::convertirPixelBN() const {
	unsigned int moyenne = somme() / 3;
	return moyenne > 127;
}

unchar PixelCouleur::convertirPixelGris() const {
	return somme() / 3;
}

void PixelCouleur::modifierTeinteRouge(unchar d) {
	donnee_[Couleur::R] = d;
}
void PixelCouleur::modifierTeinteVert(unchar d) {
	donnee_[Couleur::G] = d;
}
void PixelCouleur::modifierTeinteBleue(unchar d) {
	donnee_[Couleur::B] = d;
}

void PixelCouleur::mettreEnNegatif() {
	donnee_[Couleur::R] = 255 - donnee_[Couleur::R];
	donnee_[Couleur::G] = 255 - donnee_[Couleur::G];
	donnee_[Couleur::B] = 255 - donnee_[Couleur::B];
}

Pixel* PixelCouleur::retournerCopieProfonde() const {
	Pixel* newp = new PixelCouleur(donnee_[Couleur::R], donnee_[Couleur::G], donnee_[Couleur::B]);
	return newp;
}
