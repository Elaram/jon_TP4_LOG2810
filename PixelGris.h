//
// Created by Gabriel Bernard
//

#ifndef TP3_PIXEL_GRIS_H
#define TP3_PIXEL_GRIS_H

#include "Pixel.h"
/**
* Classe d�finissant un pixel en nuances de gris
* @author Vincent Bolduc et Jonathan Plante
* @version 2.0
*/
class PixelGris :
	virtual public Pixel
{
public:
	PixelGris();
	/**
	* Initialise un objet de classe PixelGris avec les valeurs par d�faut
	*/
	PixelGris(unsigned int);
	/**
	* Initialise un objet de classe PixelGris avec les valeurs d�finies par des param�tres
	* @param int La valeur de la teinte grise du pixel
	*/
	~PixelGris();
	/**
	* Supprime un objet de classe PixelGris
	*/
	bool convertirPixelBN() const;
	/**
	* Convertit un objet PixelGris en objet PixelBN
	* @return true si la teinte du pixel est pr�s du blanc et false si la teinte du pixel est plus pr�s du noir
	*/
	unchar* convertirPixelCouleur() const;
	/**
	* Convertit un objet PixelGris en objet PixelCouleur
	* @return ret un pointeur vers un tableau de unchar correspondant aux teintes R,G,B du pixel
	*/
	unchar convertirPixelGris() const;
	/**
	* Convertit un PixelGris en PixelGris (implementation necessaire car methode virtuelle pure heritee)
	* @return donnee_ la valeur de teinte grise du pixel
	*/
	unsigned int obtenirDonnee() const;
	/**
	* Obtient le niveau de gris du pixel
	* @return donnee_ le niveau de gris du pixel
	*/
	virtual unchar obtenirR() const;
	/**
	* Obtient la valeur de teinte rouge du pixel gris
	* @return donnee_ la valeur de teinte rouge du pixel gris
	*/
	virtual unchar obtenirG() const;
	/**
	* Obtient la valeur de teinte verte du pixel gris
	* @return donnee_ la valeur de teinte verte du pixel gris
	*/
	virtual unchar obtenirB() const;
	/**
	* Obtient la valeur de teinte bleue du pixel gris
	* @return donnee_ la valeur de teinte bleue du pixel gris
	*/
	virtual void mettreEnNegatif();
	/**
	* Inverse le niveau d'intensit� d'un pixel gris
	* @return  void(rien)
	*/
	virtual Pixel* retournerCopieProfonde() const;
	/**
	* Fait une copie profonde d'un pixel gris
	* @return  newp un pointeur vers le PixelGris nouvellement cree en memoire
	*/

private:
	unchar donnee_;
};

#endif // TP3_PIXEL_GRIS_H