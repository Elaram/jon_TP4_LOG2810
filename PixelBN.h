

#ifndef TP3_PIXELBN_H
#define TP3_PIXELBN_H

#include "Pixel.h"
#include "TypePixel.h"
/**
* Classe d�finissant un pixel noir ou blanc
* @author Vincent Bolduc et Jonathan Plante
* @version 2.0
*/
class PixelBN: virtual public Pixel
{
public:
	PixelBN();
	/**
	* Initialise un objet de classe PixelBN avec les valeurs par d�faut
	*/
	PixelBN(bool p);
	/**
	* Initialise un objet de classe PixelBN avec les valeurs d�finies par des param�tres
	* @param p La couleur (true pour blanc, false pour noir) du pixel
	*/
	~PixelBN();
	/**
	* Supprime un objet de classe PixelBN
	*/
	unchar* convertirPixelCouleur() const;
	/**
	* Convertit un objet PixelBN en objet PixelCouleur
	* @return ret un pointeur vers un tableau de caracteres representant les teintes RGB du pixel
	*/
	unchar convertirPixelGris() const;
	/**
	* Convertit un objet PixelBN en objet PixelGris
	* @return pixelGris une variable de type unchar repr�sentant la teinte noire ou blanche du nouvel objet PixelGris
	*/
	bool convertirPixelBN() const;
	/**
	* Convertit un PixelBN en PixelBN (implementation necessaire car methode virtuelle pure heritee)
	* @return donnee_ qui est vrai(true) si le pixel est blanc et faux(false) si le pixel est noir
	*/
	bool obtenirDonnee() const;
	/**
	* Obtient la valeur de couleur du pixel noir ou blanc
	* @return donnee_ dont la valeur est true si le pixel est blanc et false si le pixel est noir
	*/
	virtual unchar obtenirR() const;
	/**
	* Obtient la valeur de teinte rouge du pixel noir ou blanc
	* @return donnee_ dont la valeur est true si le pixel est blanc et false si le pixel est noir
	*/
	virtual unchar obtenirG() const;
	/**
	* Obtient la valeur de teinte verte du pixel noir ou blanc
	* @return donnee_ dont la valeur est true si le pixel est blanc et false si le pixel est noir
	*/
	virtual unchar obtenirB() const;
	/**
	* Obtient la valeur de teinte bleue du pixel noir ou blanc
	* @return donnee_ dont la valeur est true si le pixel est blanc et false si le pixel est noir
	*/
	virtual void mettreEnNegatif();
	/**
	* Inverse le niveau d'intensit� d'un pixel noir ou blanc (noir=>blanc ou blanc=>noir)
	* @return  void(rien)
	*/
	virtual Pixel* retournerCopieProfonde() const;
	/**
	* Fait une copie profonde d'un pixel noir ou blanc
	* @return  newp un pointeur vers le PixelBN nouvellement cree en memoire
	*/
private:
	bool donnee_;
};

#endif // TP3_PIXELBN_H