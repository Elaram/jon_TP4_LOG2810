//
// Created by Gabriel Bernard
//

#ifndef TP3_IMAGE_H
#define TP3_IMAGE_H

#ifndef PAUSE
    #ifdef _WIN32
        #include <Windows.h>
        #define PAUSE system("PAUSE")
    #else
        #include <unistd.h>
        #define PAUSE sleep(0)
    #endif
#endif /* PAUSE */

#include <string>
#include <fstream>

typedef unsigned char unchar;

#define VALEUR_MAX_PIXEL 255
#define VALEUR_MIN_PIXEL 0

#include "TypeImage.h"
#include "Pixel.h"
/**
* Classe d�finissant une image
* @author Vincent Bolduc et Jonathan Plante
* @version 4.0
*/
class Image {
public:
    Image();
	/**
	* Initialise un objet de classe Image avec les valeurs par d�faut
	*/
    Image(const Image& i);
	/**
	* Constructeur de copie qui ecrase les attributs d'une image par ceux de celle specifiee
	* @param i l'image dont les attributs ecraseront ceux de l'objet Image courant
	*/
    Image(const std::string& cheminDuFichier, const TypeImage& type);
	/**
	* Initialise un objet de classe Image avec les valeurs d�finies par des param�tres
	* @param cheminDuFichier le cheminsur le didque vers le fichier image
	* @param type le type de l'image(couleur, nuances de gris ou noire et blanche)
	*/
    ~Image();
	/**
	* Supprime un objet de classe Image
	*/
    std::string obtenirCheminDuFichier() const;
	/**
	* Obtient le chemin d'acces au fichier image sur le disque
	* @return cheminVersImageOriginale_ Une chaine de caracteres donnant le chemin vers le fichier image
	*/
    std::string obtenirNomImage() const;
	/**
	* Obtient le nom du fichier image
	* @return nomDuFichier_ le nom du fichier image
	*/
    unsigned int obtenirNombrePixelHauteur() const;
	/**
	* Obtient la dimensions de hauteur (en nombre de pixel) de l'image
	* @return hauteur_ la hauteur de l'image
	*/
    unsigned int obtenirNombrePixelLargeur() const;
	/**
	* Obtient la dimensions de largeur (en nombre de pixel) de l'image
	* @return largeur_ la largeur de l'image
	*/
    unsigned int obtenirTaille() const;
	/**
	* Obtient la taille de l'image (hauteur multipliee par largeur)
	* @return taille_  la taille de l'image
	*/
    TypeImage obtenirType() const;
	/**
	* Obtient le type de l'image
	* @return typeImage_ le type de l'image, un element de l'enumeration TypeImage
	*/
    std::string obtenirTypeEnString() const;
	/**
	* COnvertit le type de l'image en un objet de classe string
	* @return
	*/
    void changerNomImage(const std::string& nom);
	/**
	* Mutateur du nom de l'image
	* @param nom un string constituant le nouvau nom de l'image
	* @return nomDuFichier le nouveau nom de l'image
	*/
    void sauvegarderImage(const std::string &nomDuFichier = "./out.ppm") const;
	/**
	* Enregistre l'image sur le disque avec un nom specifie
	* @param nomDuFichier le nom du fichier image a sauvegarder
	* @return void(rien)
	*/
    void lireImage(const std::string &nomDuFichier, const TypeImage& type);
	/**
	* Effectue la lecture d'une image
	* @param nomDuFichier le nom du fichier image a lire
	* @param type le type de l'image lue
	* @return void(rien)
	*/
    // overload d'operateurs
    Image& operator=(const Image& image);
	/**
	* Surcharge de l'op�rateur d'affectation pour permettre d'ecraser les attributs d'un objet Image par ceux d'un autre
	* @param image l'image dont les attributs ecraseront ceux de l'image d'origine
	* @return  *this un pointeur vers l'objet Image dont les attributs ont ete ecrases
	*/
    bool operator==(const Image& image);
	/**
	* Surcharge de l'op�rateur bool�en pour permettre de v�rifier que deux Image sont identiques
	* @param image l'image a comparer a l'image courante
	* @return  true(vrai) si les deux images sont identiques et false(faux) des que leur nom, taille ou pixel de meme position different
	*/
    bool operator==(const std::string& nom);
	/**
	* Surcharge de l'op�rateur bool�en pour permettre de v�rifier que deux Image ont le meme nom ou qu'une image a un nom donne
	* @param nom le nom dont on veut verifier qu'il est bien celui de l'image
	* @return  true(vrai) si le nom de l'image est bien celui specifie et false(faux) sinon
	*/
    friend bool operator==(const std::string& nom,  Image& image);
	/**
	* Surcharge de l'op�rateur bool�en pour permettre de v�rifier le nom d'une image en "l'egalisant" a l'objet image
	* @param nom le nom dont on veut verifier qu'il est bien celui de l'image
	* @param image l'image dont on veut verifier le nom
	* @return  true(vrai) si le nom de l'image est bien celui specifie et false(faux) sinon
	*/
    friend std::ostream& operator<<(std::ostream& os, const Image& image);
	/**
	* Surcharge de l'op�rateur de flux de sortie pour afficher les infos sur un objet Image (une image)
	*/
    void convertirNB();
	/**
	* Methode convertissant une image en image avec des pixels uniquement noirs ou blancs
	*/
    void convertirGris();
	/**
	* Methode convertissant une image en image avec des pixels en niveaux de gris
	*/
    void convertirCouleurs();
	/**
	* Methode convertissant une image en image avec des pixels en couleur
	*/
	void toutMettreEnNegatif();
	/**
	* Methode inversant les niveaux d'intensite des teintes des pixels de l'image
	*/
private:
    void lirePixelsCouleur(std::ifstream &bmpIn);
    void lirePixelsGris(std::ifstream &bmpIn);
    void lirePixelsBN(std::ifstream &bmpIn);
    // Fonction privee qui permet de faire une copie profonde d'une image
    // passee en reference dans l'objet image actuel
    void copieProfonde(const Image& image);

    std::string cheminVersImageOriginale_;  // Chemin de lecture du fichier original
    std::string nomDuFichier_;     // Nom du fichier
    Pixel** pixels_;               // Tableau des pixels associes a l'image
    unsigned int hauteur_;         // Hauteur de l'image en pixel
    unsigned int largeur_;         // Largeur de l'image en pixel
    unsigned int taille_;          // Taile de l'image (hauteur_ * largeur_)
	TypeImage typeImage_;		   // Type de l'image cree
};

size_t bitmap_encode_rgb(const unchar *rgb, unsigned int width, unsigned int height, unsigned char** output);

#endif //TP3_IMAGE_H
